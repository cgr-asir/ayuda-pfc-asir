## Escribir Documentación

A la hora de escribir documentación tienes que **entender la diferencia** entre:

- El contenido (lo que escribes), que puede ir en texto plano, sin ningún tipo de formato más allá del uso de algún caracter especial, separadores de línea y espacios
- El formato (como aparece), que puede ser vía un procesador de texto WYSIWYG (tipo MS Word)
- El resultado final, que puede ser un PDF listo para imprimir y/o enviar, o HTML para ver en una página web

La intención aquí es:
1. Usar documentos de texto puro, para centrarnos en el contenido
2. Con una estructura y un lenguaje de marcado sencillo
3. Que te permita:
  A. Controlar las diferentes versiones del documento, sin perder nada
  B. Enviar / Recibir comentarios y/o nuevas versiones del documento
  C. Que automáticamente lo muestre en un formato bonito de ver (HTML generado automáticamente)

## Aprender Markdown

[Markdown](https://es.wikipedia.org/wiki/Markdown) es un lenguaje de marcas ligero ([hay más](https://es.wikipedia.org/wiki/Lenguaje_de_marcas_ligero)) que está integrado en gitlab ya sea usando el IDE o escribiéndolo directamente. Como es tan sencillo **es muy fácil de utilizar y ver el resultado a la vez** porque el mismo servicio web permite editar y ver el resultado. Lo único que necesitas saber es:

- Cómo resaltar texto (vía **negritas** o *cursivas*)
- Cómo crear títulos y estructura (añadiendo uno o varios # antes del título)
- Cómo hacer viñetas no numeradas:
 * Uno
 * Dos
 * Tres
- Y no numeradas:
 1. Una
 2. Dos
 3. Tres
- Cómo poner enlaces `[Wikipedia](https://www.wikipedia.org/)` -> [Wikipedia](https://www.wikipedia.org/)
- Cómo añadir imágenes (mejor usar referencias) `![alt text][logo]` -> ![alt text][logo]

Puedes revisar [Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) para una ayuda más detallada.

## Problemas con Markdown

Un problema es que **no hay una sintaxis estandarizada** y hay diferentes *sabores*. Por ejemplo, en algunas funciones de Gitlab, se usa un **sabor específico** [](https://docs.gitlab.com/ee/user/markdown.html). Pero **no te líes** para lo básico y lo que vas a utilizar aquí te llega. Y recuerda que (casi) siempre puedes ver el resultado de lo que escribes. El problema de la falta de estandarización se compensa con mucho los beneficios de utilizarlo, en la mayor parte de las ocasiones.

Sí plantea más problemas para **documentación técnica más compleja**. En ese caso ya necesitas algún lenguaje de marcado más complejo que permita montar documentación estructurada, con referencias, vínculos, personalizaciones, índices, contenidos, vínculos y más información. Pero para una documentación como la que aquí estamos manejando, *te llegará de sobra*. Tendrás que usar los directorios y diferentes archivos para separar los capítulos. Para documentación técnica más compleja yo por ejemplo prefiero [ReEStructuredText](https://es.wikipedia.org/wiki/ReStructuredText) con sphinx.
