# Ayuda PFC

Este es el proyecto principal para centralizar **toda la ayuda sobre el PFC** (Proyecto Fin de Ciclo) de ASIR. Recuerda revisar:

* la [Guía del PFC oficial del Departamento](PDFs/guia_proxecto.pdf) (en pdf)
* la [Wiki del proyecto DOC-Leliadoura](https://gitlab.com/iesleliadoura/docu/documentacion) para toda la información sobre el PFC.

Además, también puedes consultar mis materiales (no oficiales del departamento):

- [preguntas más frecuentes](https://www.apuntesinformaticafp.com/modulos/pfc.html)
- [el currículum PFC](https://www.apuntesinformaticafp.com/ciclos/modulos/pfc.html) 

# Primeros Pasos

El primer objetivo sería **crear la ficha genérica de tu proyecto**  usando la plantilla genérica. Para eso necesitas seguir estos pasos:

1. Entender cómo [escribir documentación](https://gitlab.com/cgr-asir/ayuda-pfc-asir/-/blob/main/escribir.md) (y usar Markdown)
2. [Usar Gitlab](https://gitlab.com/cgr-asir/ayuda-pfc-asir/-/blob/main/uso_gitlab.md)
3. Preparar tu entorno de desarrollo

   3.1. En local (en tu máquina)

   3.2. Vía Web IDE (con el navegador)
4. Entender lo básico de control de versiones
5. Modificar el README.md del proyecto (la página principal genérica)

Y luego puedes ir añadiendo el [anteproyecto](https://gitlab.com/iesleliadoura/docu/documentacion/-/wikis/Anteproxecto) y las demás fases, para su revisión.

# Entender el Control de versiones

El sistema de control de versiones permite hacer **un seguimiento de todo lo que ocurre** en un proyecto (repositorio), ya sea que participe una persona o varias. A la hora de gestionar documentación, te permite:
* Subir cualquier tipo de archivo
* Editar archivos de texto directamente
* Saber quien, cuando y qué se ha cambiado
* Participar y Colaborar con otros (aceptar cambios, enviar y recibir comentarios, etc)
* (pro) Instalar tu entorno de desarrollo en local (claves ssh + git en local)

Ojo que en realidad el uso de control de versiones **se usa sobre todo para el control de versiones de código** (programación), pero como los programas en código fuente son archivos de texto, sirve también para sistemas documentales (basados en texto). Con un documento de MS Word o de LibreOffice no tiene sentido (sólo como copia de seguridad).

En la plantilla del proyecto que vas a clonar ( [Plantilla Proyecto ASIR](https://gitlab.com/iesleliadoura/asir2-pfc) ) verás que hay dos directorios:

* `/doc` donde estará toda la documentación de tu proyecto
* `/src` donde está el proyecto en sí (si tienes código que va a ser supervisado con git). Aquí irían los scripts o archivos de configuración de los servicios, por ejemplo. 
* Además hay un archivo índice (README.md) que es la página principal de tu proyecto y desde donde puedes hacer referencia a todo. Piensa en este archivo como si fuera el `index.html` principal de tu sitio web

git (que es el sistema sobre el que se apoya gitlab) no se ocupa de cómo editas tú tus archivos. Puedes hacerlo con el editor de texto que te de la gana. Y hay algunos programas que te facilitan bastante, por ejemplo Visual Studio, porque ya suelen tener integrado git (y gitlab o github) directamente en el software. Sí, aunque sea para hacer documentación.

Para **los administradores de sistemas** pues un editor de texto avanzado para la consola como emacs o vim quizá te vengan mejor, pero tienen una curva de aprendizaje fuerte (y no se dan en el ciclo).

Este es **un video de casi 1 hora** pero que **explica muy bien el uso de git**. gitlab/github son herramientas que trabajan por encima de git.

   [Git y Github | Curso Práctico de Git y Github Desde Cero](https://www.youtube.com/watch?v=HiXLkL42tMU)

Y esto son unas notas de un curso de git [Controlando Versiones](https://docs.google.com/document/d/1zkkRAOzLLxYrhVkvNttDXh8hYGi6xrdgNtfhXeCgJuA/edit#heading=h.hpds5lu8cit8). Mejorable, que sólo lo di una vez. No creo que tenga tiempo antes del verano para actualizarlo. 

# Descargar la plantilla de proyecto

Ahora ya puedes comenzar a trabajar en tu proyecto usando gitlab como herramienta de control de versiones (de incidencias, de comentarios, etc), para eso tendrás que [descargar la plantilla de PFC ASIR](https://gitlab.com/iesleliadoura/asir2-pfc) que te da:
* Una estructura de archivos y directorios para comenzar a trabajar
* Una descripción de lo que debería ir en cada parte

El concepto descargar en un sistema de control de versiones implica hacer **una de estas dos acciones** (una u otra, son excluyentes):
* Clonar. En el botón de Clone podrás seleccionar un URL para clonar, en tu entorno de desarrollo local (sea con cliente o gráfico) un repositorio. En este caso vas a utilizar tu propio entorno de desarrollo (en tu máquina)
* Copiar (fork). El botón de Fork lo añade a tus proyectos y lo puedes editar directamente desde gitlab. En esta caso vas a utilizar el IDE de gitlab directamente. Es lo que **te recomiendo al principio**.

Revisa la [Plantilla Proyecto ASIR](https://gitlab.com/iesleliadoura/asir2-pfc) y comienza a trabajar con ella para generar la documentacion de tu proyecto. Los pasos a seguir son:
1. Modificas el archivo README.md
2. Editas la Idea y las necesidades (dos archivos diferentes) con la idea de tu proyecto
3. Estas modificaciones se hacen en una rama de desarrollo (o seleccionas la rama main). Una rama (branch) es como si fuera un *directorio paralelo* donde se guardan los cambios. El repositorio puede guardar todos los cambios de todos estos *directorios paralelos*.
4. Los cambios quedan registrados y esperas que lo aprueben e incluyan en la rama principal (main)
5. Y vuelves a editar

Además, **tienes el gestor de incidencias** (*issues* y *board*). Sobre tu proyecto tendremos permiso todos los profesores y podrás preguntarle a cualquiera y te podrá responder directamente sobre el documento.

# Flujo de Trabajo (de ejemplo):

Descargamos un proyecto a nuestro sistema de archivos, y vemos el histórico de cambios. Ves que añade un directorio y ahí tiene todo sus sistema de control (en un directorio .git).

![Clonar/Descargar y ver histórico][git1]

Modificamos y/o añadimos archivos nuevos. Añadir implica primero añadirlos (*add*) y luego registrarlos (*commit*) aunque se puede hacer todo a la vez (*commit -a*). Siempre es bueno revisar el estado del repositorio (*status*) y leer lo que te dice, que es bastante explicativo.

![Trabajo][git2]

Revisar el histórico de cambios, y cuando quieras subes lo que tienes en tu máquina (*origin/main, origin/HEAD*) a gitlab (*HEAD -> main*). Y sigues trabajando.

![Subir Cambios][git3]


¿Y cómo borro archivos? Pues **no los borras** (que quizá luego los necesitas), dile a git que registre el borrado (*git rm*). Parece lo mismo, pero no lo es. Una vez que git tiene el control (supervisión) de tu sistema de archivos, hazlo todo con git.


------------------

[git1]: img/git_01.png
[git2]: img/git_02.png
[git3]: img/git_03.png
