## Jugar con Gitlab

Gitlab es una [herramienta muy potente](https://www.ionos.es/digitalguide/paginas-web/desarrollo-web/tutorial-de-gitlab/) aunque vamos a comenzar a **utilizar sólo lo más básico**:

1. Gestionar las versiones de la **documentación** de tu proyecto
2. **Gestionar los comentarios** (issues). Es decir, centralizar las revisiones y tareas de tu proyecto
3. Ver la **actividad sobre tu proyecto** para evitar preguntar cómo va el proyecto (se ve en la actividad)

Puedes revisar este [Tutorial de Gitlab](https://gitlab.com/iesleliadoura/docu/documentacion/-/wikis/uploads/7421650f8958e49b57a1d5e01031666f/Tutorial_de_GitLab.pdf)

### Para empezar

Aunque es muy potente, arrancar a trabajar con gitlab es fácil. Para empezar lo que vas a hacer es:

1. Creas tu usuario gitlab (te añadirán a un grupo de trabajo)
2. Juegas con la interfaz:
 - Configuras los datos de tu perfil: Modifica tu foto, tu nombre y la zona horaria
 - Revisas los diferentes proyectos: ¿Quien es el propietario de ese proyecto? ¿Quien fue la última persona en participar? ¿Qué archivos tiene? ¿Cual es el histórico de cambios?
 - Participas en alguno (por ejemplo en este proponiéndome algún cambio en alguna parte de la documentación) o enviando una incidencia (issue)
3. Exploras proyectos. Por ejemplo ver los [proyectos del Leliadoura](https://gitlab.com/iesleliadoura). Sólo te mostrará los que tienes permiso. Pero puedes **revisar cualquier proyecto público**.

Y ya tienes todo para ponerte a trabajar. 

### Dos Formas de trabajar

A la hora de trabajar en el proyecto puedes:

* hacerlo vía web (sencillo pero menos potente) o ...
* preparar tu entorno de desarrollo (más potente y rápido, pero tienes que prepararlo)

#### Vía Web

Puedes usar el editor vía web para poder conectarte a tu proyecto desde cualquier lugar. Puedes modificar, crear, borrar cualquier archivo de tu proyecto.

Ojo que **cuando hagas un commit en la web** (lo ves más abajo) dependerá del tipo de permisos que tengas. Lo habitual es que o te aseguras de hacerlo en la rama principal (main) o, si lo creas en otra rama, luego hay que fusionarlas (merge). Depende de cómo esté la configuración de roles y permisos.

#### En tu entorno local

Te permite trabajar directamente en tu entorno de trabajo. Descargarás (*clone*) tu proyecto y trabajas en él directamente.

Vas anotando y registrando los cambios (*commit*) y cuando tengas lista una versión lo subes (*push*) a gitlab.

Si trabajas con otras personas, acuérdate de sincronizar los cambios que hayan hecho (*pull*).  Es decir, la secuencia es sencilla:

1. Clonas/Descargas (*clone/fork*) el proyecto: y ya lo tienes en tu sistema de archivos
2. Descargas los últimos cambios (*pull*)
3. Editas los archivos, o modificas el sistema de archivos (añadiendo archivos)
4. Registras los cambios en repositorio (*commit*)
5. Subes los cambios (*push*)

## Un par de videos rápidos

¿Se te hace complicado? Puedes ver este par de videos para ponerte en marcha:

* Crear tu cuenta un proyecto de prueba [Video 10'](https://www.youtube.com/watch?v=sfBwSrifMBg). Lo básico, y para trabajar sólo con el IDE. Para el objetivo aquí (documentación) quizá te llegue
* Subir y Descargar Videos desde la consola [Video 10'](https://www.youtube.com/watch?v=yVh5UwTZUKs). Más avanzado, pero mejor a la larga (lo instalas y ya sólo trabajas en tu máquina)

## Documentación Oficial

Si ya te vienes muy arriba pues revisar la documentación oficial (en inglés), donde está **todo lo que hay que saber**. De lo que vamos a trabajar aquí te puede ayudar:
1. Proyecto (Repositorios e Incidencias). [Organize work with projects](https://docs.gitlab.com/ee/user/project/index.html)
2. Seguimiento (Incidencias, Hitos, Tareas). [Plan and track work](https://docs.gitlab.com/ee/topics/plan_and_track.html)
3. Estadísticas. [Analyze GitLab usage](https://docs.gitlab.com/ee/user/analytics/index.html)

Pero no creo que te haga falta consultarlo mucho. Sólo por si quieres profundizar en algo.

